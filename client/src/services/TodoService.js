import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://localhost:5000',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
})

export default {
  fetchTodos() {
    var todos = apiClient.get('/todos')
    return todos
  },
  addTodo(todo) {
    var newTodo = apiClient.post('/todos', todo)
    return newTodo
  },
  updateTodo(payload) {
    var updatedTodo = apiClient.patch(`/todos/${payload.id}`, payload)
    return updatedTodo
  },
  deleteTodo(id) {
    return apiClient.delete(`/todos/${id}`)
  },
}
