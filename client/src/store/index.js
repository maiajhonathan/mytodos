import Vue from 'vue'
import Vuex from 'vuex'

import TodoService from '../services/TodoService'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todos: [],
  },
  mutations: {
    FETCH_TODOS(state, todos) {
      state.todos = todos.sort((a, b) => {
        return a.id - b.id
      })
    },
    ADD_TODO(state, todo) {
      state.todos.push(todo)
    },
    UPDATE_TODO(state, todo) {
      const index = state.todos
        .map(x => {
          return x.id
        })
        .indexOf(todo.id)
      if (index > -1) {
        var oldTodo = state.todos[index]
        oldTodo.description = todo.description
        oldTodo.done = todo.done
      }
    },
    DELETE_TODO(state, id) {
      const index = state.todos
        .map(x => {
          return x.id
        })
        .indexOf(id)
      if (index > -1) {
        state.todos.splice(index, 1)
      }
    },
  },
  actions: {
    async fetchTodos({ commit }) {
      await TodoService.fetchTodos()
        .then(response => {
          var todos = response.data
          commit('FETCH_TODOS', todos)
        })
        .catch(error => {
          console.log(error.message)
        })
    },
    async addTodo({ commit }, todo) {
      await TodoService.addTodo(todo)
        .then(response => {
          var newTodo = response.data
          commit('ADD_TODO', newTodo)
        })
        .catch(error => {
          console.log(error.message)
        })
    },
    async updateTodo({ commit }, todo) {
      await TodoService.updateTodo(todo)
        .then(() => {
          commit('UPDATE_TODO', todo)
        })
        .catch(error => {
          console.log(error.message)
        })
    },
    async deleteTodo({ commit }, id) {
      await TodoService.deleteTodo(id)
        .then(() => {
          commit('DELETE_TODO', id)
        })
        .catch(error => {
          console.log(error.message)
        })
    },
  },
  modules: {},
})
