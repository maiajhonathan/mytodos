const { Model, DataTypes } = require("sequelize");

class Todo extends Model {
  static init(sequelize) {
    super.init(
      {
        description: DataTypes.STRING,
        done: DataTypes.INTEGER,
      },
      {
        // DBConnection
        sequelize,
        tableName: "todos",
      }
    );
  }
}

module.exports = Todo;
