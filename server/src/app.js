const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

require("./database");

const todoRoutes = require("./routes/todo");

const PORT = process.env.PORT || 5000;

// Creates an express server.
const app = express();

// Cors config.
const corsConfig = {
  origin: "http://localhost:8080",
};

// Middleware
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors(corsConfig));

// Default route
app.get("/", (req, res) => {
  res.send({ message: "This is the default router." });
});

// Other routes
app.use(todoRoutes);

// Initiates the server.
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
