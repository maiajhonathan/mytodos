const Todo = require("../models/Todo");

module.exports = {
  async index(req, res) {
    let todos = [];
    try {
      todos = await Todo.findAll();
      return res.json(todos);
    } catch (error) {
      console.log(error);
    }
  },
  async store(req, res) {
    try {
      const { description } = req.body;
      let todo = await Todo.create({ description });
      return res.json(todo);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  },
  async update(req, res) {
    try {
      const { id } = req.params;
      const { description, done } = req.body;
      let updatedTodo = await Todo.update(
        { description: description, done: done },
        {
          where: {
            id: id,
          },
        }
      );
      return res.json(updatedTodo);
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  },
  async delete(req, res) {
    try {
      const { id } = req.params;
      await Todo.destroy({ where: { id: id } });
      return res.json({ message: `Todo id: ${id} deleted!` });
    } catch (error) {
      console.log(error);
      res.json(error);
    }
  },
};
