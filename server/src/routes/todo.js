const { Router } = require("express");
const todoController = require("../controllers/TodoController");

const router = Router();

router.get("/todos", todoController.index);
router.post("/todos", todoController.store);
router.patch("/todos/:id", todoController.update);
router.delete("/todos/:id", todoController.delete);

module.exports = router;
