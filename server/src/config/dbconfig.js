module.exports = {
  username: "test",
  password: "123456",
  host: "localhost",
  database: "tododb",
  dialect: "postgres",
  define: {
    timestamps: true,
    underscored: true,
  },
};
