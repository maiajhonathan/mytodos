const { Sequelize } = require("sequelize");
const dbConfig = require("../config/dbconfig");
const Todo = require("../models/Todo");

// Instantiates sequelize passing the connection configs.
const connection = new Sequelize(dbConfig);

// Passes the connection object to be used by the Todo model.
Todo.init(connection);

module.exports = connection;
